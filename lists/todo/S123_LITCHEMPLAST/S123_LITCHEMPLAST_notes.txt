PAPER DOI : 10.1021/acs.estlett.4c00355
LitChemPlast: An Open Database of Chemicals Measured in Plastics
Helene Wiesinger, Anna Shalin, Xinmei Huang, Armin Siegrist, Nils Plinke, Stefanie Hellweg, and Zhanyun Wang
Environmental Science & Technology Letters 2024 11 (11), 1147-1160
DOI: 10.1021/acs.estlett.4c00355
paper link https://pubs.acs.org/doi/10.1021/acs.estlett.4c00355?ref=pdf


Number : S123
Code : LITCHEMPLAST
Short Name : 3500 LitChemPlast database chemicals

Short Description : List of 3500 LitChemPlast database chemicals
Description : List of 3500 chemicals from the LitChemPlast database, as published in " An Open Database of Chemicals Measured in Plastics" by Wiesinger et. al.(2024).DOI: 10.1021/acs.estlett.4c00355.



S123 | LITCHEMPLAST | LitChemPlast database chemicals

