# NORMAN-SLE Lists Subdirectory

The "lists" subdirectory contains subfolders to trace the following:

| Subfolder | Description |
| ------ | ------ |
| [done](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/lists/done) | Lists already completed |
| [todo](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/lists/todo) | Lists that are in the queue |
| [WiP](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/lists/WiP) | Lists that are being actively worked on |

Please browse the subfolders for more!
