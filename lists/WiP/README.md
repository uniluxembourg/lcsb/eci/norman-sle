# Work In Progress on the NORMAN-SLE

The home for "Work in Progress" (WiP) lists and annotations

## WiP - Lists

- S96 ECIPFAS
- Addition to S6

## WiP - Annotations

- S80 PFASGLUEGE (Parviel / Emma / Jeff)
	- Updated annotation details sent to Jeff
- S77 FCCdb (Parviel / Emma / Jeff / Ksenia)
	- Currently waiting on Parviel / Emma
- S82 EAWAGPMT (Emma / Jeff)
	- Will be added as chemical class?
	- UFZPMT to be added similarly. 


## To-Dos next in line:

- TBD

