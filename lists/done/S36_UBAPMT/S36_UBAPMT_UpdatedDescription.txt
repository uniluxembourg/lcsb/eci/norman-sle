A list of REACH substances that could fulfil (very) Persistent, (very) Mobile and Toxic (PMT/vPvM) criteria according the UBA for the REACH regulation and also currently proposal by the EC for the CLP criteria. Derived from a research project by Norwegian Geotechnical Institute (NGI). Updated version provided by Hans Peter Arp, details in this technical note (UBA Report 126/2019) and an upcoming second revised version of that report to be released in 2022.

A list of REACH substances that could fulfil (very) Persistent, (very) Mobile and Toxic (PMT/vPvM) criteria according the UBA for the REACH regulation and also currently proposal by the EC for the CLP criteria. Derived from a research project by Norwegian Geotechnical Institute (NGI). Updated version provided by Hans Peter Arp, details in this [technical note (UBA Report 126/2019)](https://www.umweltbundesamt.de/publikationen/reach-improvement-of-guidance-methods-for-the) and an upcoming second revised version of that report to be released in 2022.

--- 
Adjustments: 
A list of REACH substances that could fulfil proposed (very) Persistent, (very) Mobile and Toxic (PMT/vPvM) criteria according to the German Environment Agency (UBA), Germany and also currently proposed by the European Commission (EC) for the Classification, Labelling and Packaging (CLP) criteria. Derived from a research project by Norwegian Geotechnical Institute (NGI). Updated version provided by Hans Peter Arp, details in this technical note (UBA Report 126/2019) and in an upcoming second revised version of that report to be released in 2022. Funding was provided by the Federal Ministry for the Environment, Nature Conservation, Building and Nuclear Safety of Germany (FKZ3719654080).

URLs: 
- UBA: https://www.umweltbundesamt.de/en
- CLP: https://echa.europa.eu/guidance-documents/guidance-on-clp
- Technical report: https://www.umweltbundesamt.de/publikationen/reach-improvement-of-guidance-methods-for-the