DUTCHUSE

source : 

Number : S110
Code : DUTCHUSE
Short Name : Dutch Prioritized Chemical Use Categories 


Short Description : List of 1700+ compounds and their use categories assigned within a Dutch prioritization exercise


Description : List of 1700+ compounds with monitoring data and their assigned use categories performed within Dutch prioritization exercise by Ad eco advies and Deltares (NORMAN Working Group 1).
The final use category was compiled from several lists, including the Watson database, RIWA, Dutch Substances of Very High Concern (SHVC), Kennisimpuls Waterkwalititeit (KIWK), WFD priority and specific pollutants (NL), NORMAN lists and some expert judgement.
The 1700 compounds were chosen based on availability of Dutch targeted monitoring data (3 different datasets) in 2022. This list is a translation of the file kindly provided by Anja Derksen, AD eco advies


S110 | DUTCHUSE |Dutch Prioritized Chemical Use Categories

