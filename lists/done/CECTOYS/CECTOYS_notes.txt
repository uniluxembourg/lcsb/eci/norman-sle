Note 1 :
a = Stockholm Convention on Persistent Organic Pollutants (Convention, S., 2009. Stockholm Convention on Persistent Organic Pollutants, in: Proposal to List Pentadecafluorooctanoic Acid (CAS No: 335-67-1, PFOA, Perfluorooctanoic Acid), Its Salts and PFOA-Related Compounds in Annexes A, B and/or C to the Stockholm Convention on Persistent Organic Pollutants. https://doi.org/10.1017/CBO9781107415324.004).
b = Rotterdam Convention (Mashimba, E., 2011. The Rotterdam Convention, in: Chemicals, Environment, Health. https://doi.org/10.1201/b11064-18.)
c = Montreal Protocol (Protocol, M., 1987. Protocol on substances that deplete the ozone layer. Int. Leg Mater. Negev, M., Berman, T., Reicher, S., Balan, S., Soehl, A., Goulden, S., Ardi, R.,
	Shammai, Y., Hadar, L., Blum, A., Diamond, M.L., 2018a. Regulation of chemicals in children's products: How U.S. and EU regulation impacts small markets. Sci. Total Environ. 616-617, 462--471)
d = Global Chemicals Outlook II (UN Environment, 2019. Global Chemicals Outlook II Synthesis Report (2019),ISBN: 978-92-807-3745-5)
e = Restriction of Hazardous Substances (RoHS) Directive 2002/95/CE (European Commission, 2011. Directive 2011/65/EU of the European parliament and of the council of 8 June 2011 - ROHS. Off. J. Eur. Union)
f = Toy Safety Directive 2009/48/EC (European Parliament, 2009. Directive 2009/48/EC of the European parliament and of the council of 18 June 2009 on the safety of toys. Off. J. Eur. Union 1–37)
g = Klinke et al. (2018),Analysis and Risk Assessment of Fragrances and Other Organic Substances in Squishy Toys
h = Candidate List of substances of very high concern for Authorisation (SVHC) (European Chemicals Agency(ECHA), 2019 - URL https://echa.europa.eu/candidate-list-table (accessed 12.10.19)
i = REACH ANNEX XVII ( EC European Commission, 2006. Regulation (EC) No 1907/2006 of the European Parliament and of the Council of 18 December 2006 concerning the Registration, Evaluation, Authorisation and Restriction of Chemicals (REACH), Official Journal of the European Union)
j = UN Environment (2016) - Overview report I: A Compilation of lists of chemicals recognised as endocrine disrupting chemicals (EDCs) or suggested as potential EDCs
k = California Proposition 65 list of chemicals, Office of Environmental Health Hazard Assessment (OEHHA), 2018 URL https://oehha.ca.gov/proposition-65/proposition-65-list (accessed 12.10.19)
l = CalSAFER – Safer consumer products management system (CalSAFER, 2019) - Candidate Chemical List, URL https://calsafer.dtsc.ca.gov/cms/search/?type=Chemical (accessed 12.10.19)
m = Washington State Children’s Safe Product Act (CSPA) (Washington State Department of Ecology, 2016) 
n = U.S. EPA – Chemical Substances Undergoing Prioritization (U.S. EPA, 2019) - United States Environmental Protection Agency (U.S. EPA), 2019, URL https://www.epa.gov/assessing-and-managing-chemicals-under-tsca/chemical-substances-undergoing-prioritization (accessed 1.11.20).
o = State of Washington Department of Ecology’s Product Testing Database (https://apps.ecology.wa.gov/ptdbreporting/)

Note 2 :
Category I   Substances of concern according to our and other studies (Substances included in regulatory lists of concern with HI > 1 and/or CCR > 10^-6)
Category II  Substances of concern according to our study (Substances not included in regulatory lists of concern with HI > 1 and/or CCR > 10^-6)
Category III Substances of concern according to other studies (Substances included in regulatory lists of concern with HI ≤ 1 and CCR ≤ 10^-6)
Category IV  Substances that could not be characterized in our study (Substances included in our regulatory lists of concern but without exposure/toxicity estimates)

Note 3 :
category_risk 1 = HI >10
category_risk 2 = 1< HI ≤  10
category_risk 3 = 0.1< HI ≤ 1
category_risk 4 = HI ≤ 0.1

Note 4 :
category_risk 1 = CCR > 10^-5
category_risk 2 = 10^-5 < CCR ≤  10^-6
category_risk 3 = 10^-7 <  CCR ≤ 10^-6
category_risk 4 = CCR ≤ 10^-7 

