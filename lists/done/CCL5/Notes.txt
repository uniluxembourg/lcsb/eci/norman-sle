https://www.epa.gov/ccl (the CCLs are noted towards end of this page)

These chemicals can be added via the DTXSID 
https://www.epa.gov/Node/269859
https://www.epa.gov/Node/269871
=> https://comptox.epa.gov/dashboard/dsstoxdb/batch_search 

Number: S83
Code: CCL5
Short Name: Contaminant Candidate List CCL 5 (Draft)
Description:  The CCL is a list of contaminants that are currently not subject to any proposed or promulgated US primary drinking water regulations, but are known or anticipated to occur in public water systems. From: https://www.epa.gov/ccl/contaminant-candidate-list-5-ccl-5. Dataset DOI: 10.5281/zenodo.5533801


S83 | CCL5 | Contaminant Candidate List CCL 5 (Draft)

