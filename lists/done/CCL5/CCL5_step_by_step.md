**A] SOURCE:**

	 https://www.epa.gov/ccl/draft-ccl-5-chemicals
		cmpd info available : Name, CASRN, DTXSID

**B] SLE LIST CURATION STEPS:**


	1. Create CCL5_working.xlsx with available info

	2. Verify entries (CCL5_workup tab)
  	
	  !!!Make Notes.txt/.md for any changes made!!!

		*Look for multiple entries in single row and add missing
			eg: 4-Nonylphenol (all isomers) 
	  		additional entries added 4-Nonylphenol, 3-Nonylphenol, 2-Nonylphenol (??source)
			!! Isomers have same CASRN
	  		Do reverse comptox search of DTXSID->CASRN and compare with provided CASRN.
	  		
		*Disinfection byproducts (DBPs) 
			follow hyperlink, add each cmpd, include class and subclass as new columns (useful for pubchem)
			DBP_names Tab added for reference
	 		If too many compounds eg:PFAS too complicated, skip since PFAS separate list exists

		*Any synonyms (in brackets) add to subsequent column
         	 eg: alpha-Hexachlorocyclohexane (alpha-HCH)
 	

	3. Comptox batch search using DTXSID (Add Comptox tab)

		Refer NORMAN-SLE\docs\fig\CompTox_Batch_Options
			Spot any non standard InChi string starting with '1/' instead of '1S/'
	 	 	Regenerate InChi from SMILES in OpenBabel 

	4. Pubchem identifier search (Add Pubchem tab)

		SMILES-> Pubchem ID
		Verify no missing ones -> Manual search on Pubchem and add manually
		!!Take LOWEST CID incase of multiple

	5. Create mapping file for Jeff in new tab by copying  DTXSID -> InChi from comptox search result



****HERE LIST CURATION IS COMPLETE**** 


**C]Prep for List update**

	Add short list description from SOURCE website
	Number the list (S+ next number in series), add code and short Name

**D] ZENODO**

	Personal repo for lists : Zenodo, use generated DOI in NORMAN update
	Upload .xls & .csv version of list, text files for DTXSIDs & InchiKeys

**REMINDER: !! COMMIT files on GIT!!**

**E] PUBCHEM update**

	Update NORMAN_SLE_mapping.xls on pubchem git:  deposition/NORMAN_SLE_classification.xlsx
	Add the lists number S"_" as new row
	save as Tab delimited text for Jeff
	Ref emma email Wed 9/29/2021 8:32 AM-> To Zhang, Jian (Jeff) cc:Bolton, Evan 
	

**F] NORMAN update**

	Word doc format, Add zenodo DOI to description
	Hyperlink to filename as comments
	Email Natalia (ref email emma Wed 9/29/2021 8:01 AM)
		* Word doc 'SuspectExchangeUpdate_20210928' in NORMAN-SLE\SLE_web_updates
		* .xls
		* .csv
		* Inchikeys.txt

**G] COMPTOX update**

	Ref Email Wed 9/29/2021 8:21 AM To Williams,Antony (Tony)
	Send DTXSIDs.txt attached and List description + DOI  
	








	
	


