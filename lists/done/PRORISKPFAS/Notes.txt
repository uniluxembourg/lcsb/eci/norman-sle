PRORISKPFAS

Source : Kelsey Ng, Environmental Institute, Slovakia

Number : S89
Code : PRORISKPFAS
Short Description : List of PFAS Compiled from NORMAN-SusDat

Description: A compiled list of 4777 PFAS compounds occuring in NORMAN SusDat, merging SLE lists S9, S14, S25, S46, and S80 - provided by Kelsey Ng, EI (manuscript in prep.). 

Long Description: A compiled list of 4777 PFAS compounds occuring in NORMAN SusDat, created by merging SLE lists S9, S14, S25, S46, and S80 and searching for additional fluorinated content in SusDat. Provided by Kelsey Ng, EI. Manuscript in preparation entitled: "Target and suspect screening of PFAS in wastewater, river water, ground water and biota samples in the Danube River Basin"

Author details: 
Kelsey Ng, Nikiforos Alygizakis and Jaroslav Slobodnik
Environmental Institute (EI), Slovakia

Mapping to CID and DTXSID provided by ECI.

S89 | PRORISKPFAS | List of PFAS Compiled from NORMAN-SusDat




