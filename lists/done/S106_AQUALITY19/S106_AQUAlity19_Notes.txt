S106_AQUAlity19

Source : https://www.aquality-etn.eu/

DOI: 

Number : S106
Code : AQUAlity19
Short Name :  EU AQUAlity's water analysis list 2019

Short Description :  List of compounds found in surface and wastewater sampling within the European project AQUAlity2019

Description:   List of compounds found in surface and wastewater sampling and analysis by LC-HRMS (Orbitrap) within the European project AQUAlity in 2019.
AQUAlity is a project funded by the European Union under the Marie Skłodowska-Curie Actions (MSCA) –  Innovative Training Networks (Call: H2020-MSCA-ITN-2017). Project N. 765860.
 The list was kindly provided by Azziz Assoumani (INERIS, France)


S106 | AQUAlity19 |  EU AQUAlity's water analysis list 2019