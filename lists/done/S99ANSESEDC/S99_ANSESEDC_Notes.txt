ANSESEDC

Source : https://www.anses.fr/en/content/accelerating-assessment-endocrine-disruptors
                Accelerating the assessment of endocrine disruptors | Anses - Agence nationale de sécurité sanitaire de l’alimentation, de l’environnement et du travail


Number : S99
Code : ANSESEDC
Title : List of potential endocrine disrupting compounds (EDCs) from ANSES 

Short Description: A list of 908 potential endocrine disrupting compounds (EDCs) from ANSES (https://www.anses.fr/en/) to be assessed for their endocrine disrupting properties (https://www.anses.fr/en/content/accelerating-assessment-endocrine-disruptors) under France’s Second National Endocrine Disruptor Strategy (SNPE 2). List kindly provided by Sandrine Andres, INERIS. 

Description: A list of 908 potential endocrine disrupting compounds (EDCs) from ANSES, the French Agency for Food, Environmental and Occupational Health & Safety (https://www.anses.fr/en/) to be assessed for their endocrine disrupting properties (https://www.anses.fr/en/content/accelerating-assessment-endocrine-disruptors) under France’s Second National Endocrine Disruptor Strategy (SNPE 2). Endocrine disruptors (EDs) are substances that interfere with the hormonal functions of humans and animals. List kindly provided by Sandrine Andres, INERIS. 




S99 | ANSESEDC | List of potential endocrine disrupting compounds (EDCs) from ANSES



ANSES is the French Agency for Food, Environmental and Occupational Health & Safety.

It is a public administrative body reporting to the Ministries of Health, the Environment, Agriculture, Labour and Consumer Affairs.



