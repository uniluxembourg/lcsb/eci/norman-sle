# A collection of documentation for the NORMAN-SLE

This [docs](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/tree/master/docs) 
subfolder contains useful documentation and tips to use the lists 
on the NORMAN Suspect List Exchange ([NORMAN-SLE](https://www.norman-network.com/nds/SLE/)).
The contents of this folder will grow progressively as we add tips and tricks and ideas. 

Do you have an idea? Please email the [ECI NORMAN-SLE team](mailto:normansle@uni.lu)!

## Documentation Menu / Overview

| File | Description |
| ------ | ------ |
| [CitingSLE](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/blob/master/docs/CitingSLE.md) | Citing NORMAN-SLE Lists |
| [Credits](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/blob/master/docs/CreditsSLE.md) | NORMAN-SLE Credits: Coordinators & Contributors |
| [License](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/blob/master/docs/LicenseSLE.md) | License Details for NORMAN-SLE Lists |
| [SLEtoSDF](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/blob/master/docs/SLEtoSDF.md) | Converting NORMAN-SLE lists to SDF via PubChem |
| [SLEwithCCS](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/blob/master/docs/SLEwithCCS.md) | Finding CCS Values for NORMAN-SLE lists via PubChem |
| [SLEwithMS](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/blob/master/docs/SLEwithMS.md) | Finding MS data for NORMAN-SLE lists via PubChem |
| [Updates](https://gitlab.com/uniluxembourg/lcsb/eci/norman-sle/-/blob/master/docs/UpdatesSLE.md) | Updates for the NORMAN-SLE |


