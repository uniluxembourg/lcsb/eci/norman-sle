# Updates for the NORMAN-SLE

## Updates

| Date | Update |
| ------ | ------ |
| Jan. 2025 | S122 PBTPFASNTA added, documentation updated |
| Dec. 2024 | S121 EAWAGBBD added |
| Sept. 2024 | S119 PLASTCHEM and S120 DUSTCT2024 added |
| Apr. 2024 | S116 REFCCS, S117 PFASFCCDB and S118 PFASFCCMIGEX added |
| Jan-Feb. 2024 | S112 FCCMIGEX, S113 SWISSPHARMA24, S114 SLUAMTPS and S115 EIEQSDWLGC added |
| Aug-Oct. 2024 | S108 SINLIST, S109 PARCEDC, S110 DUTCHUSE and S111 PMTPFAS added |
| Jul. 2023 | S106 AQUAlity19 and S107 AQUAlity20 added |
| Apr. 2023 | S104 UKVETMED and S105 P65CHEM added |
| Jan. 2023 | S102 PARCPFAS and S103 NORMANUVCB added |
| Nov-Dec. 2022 | S99 ANSESEDC, S100 PFASREACH and S101 MTMDUST added |
| __Oct. 2022__ | __NORMAN-SLE article published! DOI:[10.1186/s12302-022-00680-6](https://doi.org/10.1186/s12302-022-00680-6)__ |
| May-Jun. 2022 | Temporary pause to updates ... new lists coming soon! |
| Apr. 2022 | S97 UBABPAALT and S98 TIRECHEM added |
| Mar. 2022 | S95 PFASANEXCH and S96 ECIPFAS added |
| Feb. 2022 | S92 FLUOROPHARMA, S93 CECMOUTHING and S94 FLUOROPEST added |
| Jan. 2022 | S90 ZEROPMBOX1 and S91 CECTOYS added |
| Dec. 2021 | NORMAN-SLE Documentation now on [GitLab](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/docs) |
| Dec. 2021 | S87 CHLORINETPS, S88 UBABIOCIDES and S89 PRORISKPFAS added |
| Nov. 2021 | S85 MICROCYSTINS and S86 TATTOOINK added |
| Sept. 2021 | S81 through to S84 added |
| Feb-Jun. 2021 | S75 through to S80 added |
| Dec. 2020 | Updated [Transformations Tables](https://pubchem.ncbi.nlm.nih.gov/compound/22563#section=Transformations) in PubChem|
| Nov. 2020 | S74 REFTPs and S73 MetXBioDB added |
| ... | ... |

