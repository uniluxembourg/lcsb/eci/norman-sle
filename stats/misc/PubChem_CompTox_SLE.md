# Overlap of NORMAN-SLE and CompTox via PubChem

This provides a step-by-step guide to calculating the overlap between the 
[NORMAN-SLE](https://www.norman-network.com/nds/SLE/) and 
[CompTox](https://comptox.epa.gov/dashboard/) content via the 
[PubChem Classification Browser](https://pubchem.ncbi.nlm.nih.gov/classification/).
This is done via the PubChem [NORMAN-SLE Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101)
and the PubChem [EPA DSSTox Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=105).

## Overlap between CompTox Lists and NORMAN-SLE lists
This was done using the following steps: 

- Go to the PubChem [NORMAN-SLE Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101).
- Go to the PubChem [EPA DSSTox Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=105) in a separate page.
- Click on the number at the top of the [EPA DSSTox Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=105)
(142,251 as of 11 July 2022) and send this to PubChem Search. 
- Once in PubChem Search, click on the "Push to Entrez" option on the right (below "Download"). This opens a new window.
- Return to the PubChem [NORMAN-SLE Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101) and refresh the browser tab. 
A "Filter be Entrez History" dropdown menu will appear at the top, choose the number that matches the query just sent. 
- This limits the compounds shown to those also in the [EPA DSSTox Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=105). 
As of 11 July 2022, this is 71,958 CIDs. Without filtering, 115,260 CIDs are associated with the NORMAN-SLE in PubChem, the difference is 43,302.
- Send the overlap CIDs (71,958) to Entrez (PubChem Search => Push to Entrez).
- Send the NORMAN-SLE CIDs (115,260) to Entrez (remove the filter option first). 
- In the [advanced query builder](https://www.ncbi.nlm.nih.gov/pccompound/advanced) build a query that includes 
the NORMAN-SLE CIDs but not those that were in CompTox. This yields the 43,302 CIDs not in CompTox Lists. 
- In the resulting Entrez tab, click on the "View or Download Structures in PubChem" to get the download file. 
The download file includes the sources within PubChem. 
- Return to the PubChem [NORMAN-SLE Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101) and refresh again. 
- Filter by the Entrez history of the 43,302 CIDs to browse the origins (see Figure 1). 

<img src="fig/SLEnotinCompTox.png" width="70%" height="70%">

_Figure 1: Screenshot of the NORMAN-SLE Classification Browser filtered by entries not in CompTox (11 July 2022)_


## Lists With Many Entries Missing

The lists with the most entries missing (>~1000) include: 

| List | # CIDs** |
|------|------|
| S0 SUSDAT | 33,417 |
| S2 STOFFIDENT | 2,524 |
| S13 EUCOSMETICS* | 1,424/598 |
| S17 KEMIMARKET | 4,607 |
| S19 MZCLOUD | 4,104 |
| S29 PHYTOTOXINS | 935 |
| S32 REACH2017 | 16,369 |
| S33 SOLUTIONSMLOS | 1,543 |
| S55 ZINCPHARMA | 1,884 |
| S71 CECSCREEN | 6,654 |
| S73 MetXBioDB | 1,690 |
| S75 CynaoMetDB | 2,027 |
| S87 CHLORINETPS | 982 |
| S89 PRORISKPFAS | 891 |

*Note: EUCOSMETICS is integrated in two different ways ... one is a more "generous" mapping. 

**Note: CID = PubChem Compound Identifier 


## Investigating the Download File:

The download file can be used to interrogate the reasons further, as there are a number of caveats to this mapping:

- Sort by "annothitcnt" then "annothit" to find out how much annotation is available (and what it is). 
- Search for "EPA DssTox" in "sidsrcname" - this reveals that 4945 CIDs are in CompTox, but not in CompTox lists. 
This could indicate a mismatch between NORMAN-SLE and "NORMAN-SLE on CompTox" lists for these CIDs. 
- Note that the latest PubChem deposition of CompTox is out-of-date (2020-11-20; previous CompTox release)
- Approx. 5400 of the CIDs not in EPA DSSTox have at least annothitcnt >= 6. 
- Only 8246 of the CIDs not in EPA DSSTox have annothitcnt 1 (Classification, they are all in the SLE tree). 
- Latest CompTox list update on PubChem was quite recent (28 April 2022); 
updates since then on CompTox do not affect NORMAN-SLE lists.



## Credits:
Document created 11 July 2022 by E. Schymanski.

Questions about NORMAN-SLE and DSSTox Trees in PubChem: E. Bolton, J. Zhang, P. Thiessen, E. Schymanski.
Contact email for questions: [NLM/NCBI List pubchem-help](mailto:pubchem-help@ncbi.nlm.nih.gov).
