# Summary statistics for the NORMAN-SLE

A summary of statistics over all lists from 
[https://www.norman-network.com/nds/SLE/](https://www.norman-network.com/nds/SLE/) 
and
[https://zenodo.org/communities/norman-sle](https://zenodo.org/communities/norman-sle).
Per list breakdowns are provided in the files under "More details".

## NORMAN-SLE Summary

| Category | Number | Comment |
|----------|-----------|---------|
|Number of Lists | 124 | S0 to S123 |
|Total Unique Compounds | 131,572 | From [PubChem NORMAN-SLE](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101) Tree |
|Total Live Substances | 135,438 | From [PubChem NORMAN-SLE Source](https://pubchem.ncbi.nlm.nih.gov/source/23819) Page |
|Total Live Annotations | 40,610 | From [PubChem NORMAN-SLE Source](https://pubchem.ncbi.nlm.nih.gov/source/23819) Page |
|Largest List | 120,514 | S0 NORMAN-SusDat |
|Total Views | 192,666 | From [Zenodo](https://zenodo.org/communities/norman-sle) (see below) |
|Total Downloads | 168,673 | From [Zenodo](https://zenodo.org/communities/norman-sle) (see below) |
|Citations | 255 | From [Zenodo](https://zenodo.org/communities/norman-sle) (see below) |

Last updated 11 Feb. 2025.


## Zenodo Statistics Summary

| Category |11/02/2025|16/04/2024|04/04/2023| 28/04/2022 |
|----------|----------|----------|----------| ----------|
|Total Views |192,666| 143,734 | 86,005| 47,570 |
|Total Downloads |168,673| 113,615 | 97,197 | 68,569 |
|Citations |255| 230 | 61 | 40 |

More details in 
[NORMAN-SLE_Zenodo_stats_20220428.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_stats_20220428.csv),
[NORMAN-SLE_Zenodo_stats_20230404.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_stats_20230404.csv), 
[NORMAN-SLE_Zenodo_stats_20240416.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_stats_20240416.csv)and 
[NORMAN-SLE_Zenodo_stats_20250131.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_stats_20250131.csv).
Note that the unique view/download statistics were discontinued on Zenodo in Oct. 2023.


## Most Viewed / Downloaded

Top 10 viewed/downloaded lists are given in the table below, 
with the data shown per list in Figure 1. 

| List | Code | Total Views | Total Downloads | Citations |
|------|------|--------------|------------------|-----------|
| S13 | [EUCOSMETICS](https://doi.org/10.5281/zenodo.2624118) | 25,125 | 18,680 | 6 |
| S73 | [METXBIODB](https://doi.org/10.5281/zenodo.4056560) | 10,411 | 3813 | 4 |
| S00 | [SUSDAT](https://doi.org/10.5281/zenodo.10510477) | 7542 | 5372 | 23 |
| S60 | [SWISSPEST](https://doi.org/10.5281/zenodo.3544759) | 6668 | 5626 | 4 |
| S75 | [CYANOMETDB](https://doi.org/10.5281/zenodo.4551528) | 6426 | 5391 | 5 |
| S72 | [NTUPHTW](https://doi.org/10.5281/zenodo.3955664) | 6114 | 4966 | 2 |
| S25 | [OECDPFAS](https://doi.org/10.5281/zenodo.2648775) | 4944 | 3002 | 10 |
| S50 | [CCSCOMPEND](https://doi.org/10.5281/zenodo.2658162) | 4800 | 1888 | 1 |
| S29 | [PHYTOTOXINS](https://doi.org/10.5281/zenodo.2652993) | 4541 | 1422 | 2 |
| S29 | [REFTPS](https://doi.org/10.5281/zenodo.14758395) | 4287 | 2199| 5 |


Table based on total downloads/views from Zenodo, 31/01/2025. Figure based on data from 16/04/2024, 04/04/2023 and 28/04/2022.

<img src="misc/fig/SLE_downloads_views_2022vs2023vs2024.png">

_Figure 1: NORMAN-SLE Total Downloads / Views per List (Apr. 2022 vs 2023 vs 2024)._


## Zenodo Citation Summary 

| Category | 04/04/2023 | 01/05/2022 |
|----------|------------|------------|
|Total Citations | 61 | 40 |
|Total Lists Cited | 34 | 24 |
|Total Citing Articles | - | 19 |
|Internal Citing Articles | - | 12 |
|External Citing Articles | - | 7 |

More details in 
[NORMAN-SLE_Zenodo_Citations_20220501.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_Citations_20220501.csv) 
and 
[NORMAN-SLE_Zenodo_Citations_DOIs.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_Citations_DOIs.csv).
NB: A citation is considered "internal" if it involves co-authors who have contributed directly to the NORMAN-SLE.

